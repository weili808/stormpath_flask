#!/usr/bin/env python
from flask import request, session, render_template, Flask
from os import environ
import os
from stormpath.client import Client


# Create the client obj based on apikey and and apiid
client = Client(
  id=environ['STORMPATH_CLIENT_APIKEY_ID'],
  secret=environ['STORMPATH_CLIENT_APIKEY_SECRET']
)

# Create the stormpath application
application = client.applications.get(environ['STORMPATH_APPLICATION_HREF'])
app_name = application.name

# Create the flask application
app = Flask(__name__)


@app.route('/', methods=["GET"])
def home():
	if not session.get('logged_in'):
		return render_template('login.html', title=app_name)
	else:
		return "You are already logged in <a href='/logout'>Logout</a>"

@app.route("/logout")
def logout():
	if session.get('logged_in'):
		session['logged_in'] = False
    	return home()

@app.route('/signup')
def signup():
	return render_template('signup.html', title=app_name)

@app.route('/forgotpass')
def forgotpass():
	return render_template('forgotpass.html', title=app_name)


@app.route('/create_account', methods=['POST'])
def create_account():
	given_name = request.form['given_name']
        surname = request.form['surname']
	username = request.form['username']
        password = request.form['password']
	email = request.form['email']
	fav_color = request.form['fav_color']
        
	try:
		account = application.accounts.create({
			'given_name': given_name,
  			'surname': surname,
  			'username': username,
  			'email': email,
  			'password': password,
  			'custom_data': {
    				'favorite_color': fav_color
  			}
    		})
	except Exception as e:
		return str(e)+" <a href='/signup'>Back to Sign up</a>"

	return 'Created Account: {} '.format(account.email)+" <a href='/'>Back Home</a>"


@app.route('/sendmail', methods=['POST'])
def sendmail():
	email = request.form['email']
        try:
		account = application.accounts.search({'email': email})[0]
                account = application.send_password_reset_email(account.email)
	except Exception as e:
		if str(e) == 'list index out of range':
			return '{} is not found'.format(email)+" <a href='/forgotpass'>Back to Send Mail</a>"
		else:
			return str(e)+" <a href='/forgotpass'>Back to Send Mail</a>"
	
	full_name = '{} {}'.format(account.given_name, account.surname)
	
	#Token is in the mail
	#account = application.reset_account_password('TOKEN', 'newpassword')
	
	return 'Found the account for {}. Please go to "{}" to reset password'.format(full_name, account.email)+" <a href='/'>Back Home</a>"
   

@app.route('/login', methods=['POST'])
def authenticate_account():
	email = str(request.form['email'])
    	password = str(request.form['password'])
	try:
		resp = application.authenticate_account(email, password)
		session['logged_in'] = True
	except Exception as e:
		return str(e)+" <a href='/'>Back Home</a>"
        
	#print dir(resp.account)
	#print resp.account.custom_data
      	
	return "Hello {}!  <a href='/logout'>Logout</a>".format(resp.account.given_name)


	
if __name__ == '__main__':
	app.secret_key = os.urandom(12)
    	app.run(threaded=True, host='0.0.0.0', debug=True, port=80)
